# Tetris #

Whoa ... how original ... Yet another tetris game ...

More seriously, I programmed this one as a training a few years ago in pure strict C, and I am still happy with the result !

### Getting / Compiling / Installing ###

Required libraries :

* SDL
* SDL_image
* SDL_mixer
* SDL_ttf
* Your brain

Clone the repository, open Tetris.sln in Visual Studio >= 2008, compile. DLLs are shipped within the Debug directory.

Note : a binary is already provided inside the Debug directory and works great under Wine OS X.