/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/

#include "main.h"
#include "button.h"

Button::Button ()	{
	if ( !(btnLeft = IMG_Load ( "./pictures/buttons/buttonLeftPart.png" )) )
		printf ( "Unable to load file \"./pictures/buttons/buttonLeftPart.png\".\n" );
	if ( !(btnMiddle = IMG_Load ( "./pictures/buttons/buttonMiddlePart.png" )) )
		printf ( "Unable to load file \"./pictures/buttons/buttonMiddlePart.png\".\n" );
	if ( !(btnRight = IMG_Load ( "./pictures/buttons/buttonRightPart.png" )) )
		printf ( "Unable to load file \"./pictures/buttons/buttonRightPart.png\".\n" );
}

Button::~Button()	{
	if ( btnLeft )
		SDL_FreeSurface ( btnLeft );
	if ( btnMiddle )
		SDL_FreeSurface ( btnMiddle );
	if ( btnRight )
		SDL_FreeSurface ( btnRight );
}

void Button::renderButton ( SDL_Surface * dst, int x, int y, int width )	{
	SDL_Rect p = {0};

	if ( !dst )
		dst = SDL_GetVideoSurface ();

	p.x = x;	p.y = y;

	// Left part of the button
	SDL_BlitSurface ( btnLeft, NULL, dst, &p );

	// The "corpse" of the button
	for ( p.x = x + btnLeft->w; p.x <= x + btnLeft->w + width; p.x++ )
		SDL_BlitSurface ( btnMiddle, NULL, dst, &p );

	// Right part
	SDL_BlitSurface ( btnRight, NULL, dst, &p );
}

void Button::renderButton(SDL_Surface *dst, int x, int y, char *text, TTF_Font *font)	{
	SDL_Surface *textSurf;
	SDL_Rect p = {0};
	SDL_Color black = {0};

	if ( !(textSurf = TTF_RenderText_Blended ( font, text, black )) )	{
		printf ( "Unable to render \"%s\" text.\n", text );
		return;
	}

	if ( !dst )
		dst = SDL_GetVideoSurface ();
	
	renderButton ( dst, x, y, textSurf->w );
	p.x = x + btnLeft->w;	p.y = y + btnRight->h / 2 - textSurf->h / 2;
	SDL_BlitSurface ( textSurf, NULL, dst, &p );
}