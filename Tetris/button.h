/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/

#ifndef _BUTTON_H
#define _BUTTON_H

class Button	{
public:
	Button ();
	~Button ();

	// dst is destination. set it to NULL to blit on the screen
	void renderButton ( SDL_Surface * dst, int x, int y, int width = 100 );
	void renderButton ( SDL_Surface * dst, int x, int y, char * text, TTF_Font * font );

private:
	SDL_Surface * btnLeft;
	SDL_Surface * btnMiddle;
	SDL_Surface * btnRight;
};

#endif