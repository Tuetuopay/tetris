/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/


#include "main.h"
#include "menu.h"
#include "jeu.h"
#include "system.h"

#define OPTION_YES	0
#define OPTION_NO	1

/* Menus */
void Game_StartGameMenus ()
{
	Menu *mainMenu = NULL;
	SDL_Surface *ecran = SDL_GetVideoSurface (),
		*background = NULL;
	int buttonClicked = -1;

	background = Game_ConvertToVRAM ( IMG_Load ( "./pictures/background-main-menu.png" ), 1 );

	mainMenu = Menu_CreateMenu ( MENU_W, MENU_H, MENU_X, MENU_Y, 4 );
	Menu_SetElement ( mainMenu, 0, MENU_STANDARD_BUTTON, "Jouer" );
	Menu_SetElement ( mainMenu, 1, MENU_STANDARD_BUTTON, "Options" );
	Menu_SetElement ( mainMenu, 3, MENU_STANDARD_BUTTON, "Quitter" );

	while ( buttonClicked != 3 )	{
		buttonClicked = Menu_TypicMenuLoop ( mainMenu, background, ecran, 60 );

		switch ( buttonClicked )
		{
		case 0:
			Game_Menu_Play ();
			break;
		case 1:
			Game_Menu_Options ();
			break;
		}
	}

	Menu_DeleteMenu ( mainMenu );
	SDL_FreeSurface ( background );
}

void Game_Menu_Play ()
{
	Menu *playMenu = NULL;
	SDL_Surface *background = NULL;
	int buttonClicked = -1;

	playMenu = Menu_CreateMenu ( MENU_W, MENU_H, MENU_X, MENU_Y, 5 );
	Menu_SetElement ( playMenu, 0, MENU_STANDARD_BUTTON, "Facile" );
	Menu_SetElement ( playMenu, 1, MENU_STANDARD_BUTTON, "Moyen" );
	Menu_SetElement ( playMenu, 2, MENU_STANDARD_BUTTON, "Difficile" );
	Menu_SetElement ( playMenu, 4, MENU_STANDARD_BUTTON, "Retour" );

	background = IMG_Load ( "./pictures/background-main-menu.png" );

	while ( buttonClicked != 4 )	{
		buttonClicked = Menu_TypicMenuLoop ( playMenu, background, SDL_GetVideoSurface (), 60 );
		switch ( buttonClicked )
		{
		case 0:
		case 1:
		case 2:
			Game_PlayGame ( buttonClicked );
			break;
		}
	}

	Menu_DeleteMenu ( playMenu );
	SDL_FreeSurface ( background );
}

void Game_Menu_Options ()
{
	Menu *playMenu = NULL;
	SDL_Surface *background = NULL;
	FILE *fOptions = NULL;
	int buttonClicked = -1;

	playMenu = Menu_CreateMenu ( MENU_W, MENU_H, MENU_X, MENU_Y, 7 );
	Menu_SetElement ( playMenu, 0, MENU_ON_OFF, "Sons" );
	Menu_SetElement ( playMenu, 1, MENU_ON_OFF, "Musique" );
	Menu_SetElement ( playMenu, 2, MENU_ON_OFF, "Bloc fant�me" );
	Menu_SetElement ( playMenu, 3, MENU_ON_OFF, "Hautes performances" );
	Menu_SetElement ( playMenu, 4, MENU_STANDARD_BUTTON, "Contr�les..." );
	Menu_SetElement ( playMenu, 6, MENU_STANDARD_BUTTON, "Retour" );

	if ( !(fOptions = fopen ( "./options.ini", "r" )) )	{
		fclose ( fopen ( "./options.ini", "w" ) );
		//Game_SaveOptions ( OPTION_YES, OPTION_YES );
		Game_SaveGameOption ( "EnableGameSounds", OPTION_YES );
		Game_SaveGameOption ( "EnableGameMusic", OPTION_YES );
		Game_SaveGameOption ( "EnableGhost", OPTION_YES );
		Game_SaveGameOption ( "EnableHiPerf", OPTION_NO );
	}
	else
		fclose ( fOptions );
	playMenu->elements[0].toggled = Game_GetGameOption ( "EnableGameSounds" );
	playMenu->elements[1].toggled = Game_GetGameOption ( "EnableGameMusic" );
	playMenu->elements[2].toggled = Game_GetGameOption ( "EnableGhost" );
	playMenu->elements[3].toggled = Game_GetGameOption ( "EnableHiPerf" );

	background = SDL_DisplayFormatAlpha ( IMG_Load ( "./pictures/background-options.png" ) );

	while ( buttonClicked != 6 )	{
		buttonClicked = Menu_TypicMenuLoop ( playMenu, background, SDL_GetVideoSurface (), 60 );
		switch ( buttonClicked )
		{
		case 0:
			break;
		case 1:
			break;
		case 4:
			Game_Menu_OptionsKeys ();
			break;
		case 6:
			//Game_SaveOptions ( playMenu->elements[1].toggled, playMenu->elements[0].toggled );
			Game_SaveGameOption ( "EnableGameSounds", playMenu->elements[0].toggled );
			Game_SaveGameOption ( "EnableGameMusic", playMenu->elements[1].toggled );
			Game_SaveGameOption ( "EnableGhost", playMenu->elements[2].toggled );
			Game_SaveGameOption ( "EnableHiPerf", playMenu->elements[3].toggled );
			if ( playMenu->elements[1].toggled )	Mix_HaltMusic ();
			break;
		}
	}

	Menu_DeleteMenu ( playMenu );
	SDL_FreeSurface ( background );
}

void Game_Menu_OptionsKeys ()
{
	Menu *menu = NULL;
	SDL_Surface *bg = NULL;
	SDL_Event event;
	int buttonClicked = 0;
	char buf[100] = "";

	bg = Game_ConvertToVRAM ( IMG_Load ( "./pictures/background-blank.png" ), 1 );

	menu = Menu_CreateMenu ( 380, 380, 30, 30, 7 );

	sprintf ( buf, "Gauche: %d", Sys_GetIntValueInFile ( "./options.ini", "Key_left" ));
	Menu_SetElement ( menu, 0, MENU_STANDARD_BUTTON, buf );
	sprintf ( buf, "Droite %d", Sys_GetIntValueInFile ( "./options.ini", "Key_right" ));
	Menu_SetElement ( menu, 1, MENU_STANDARD_BUTTON, buf );
	sprintf ( buf, "Drop: %d", Sys_GetIntValueInFile ( "./options.ini", "Key_drop" ));
	Menu_SetElement ( menu, 2, MENU_STANDARD_BUTTON, buf );
	sprintf ( buf, "Rotation: %d", Sys_GetIntValueInFile ( "./options.ini", "Key_rotation" ));
	Menu_SetElement ( menu, 3, MENU_STANDARD_BUTTON, buf );
	sprintf ( buf, "Pause: %d", Sys_GetIntValueInFile ( "./options.ini", "Key_pause" ));
	Menu_SetElement ( menu, 4, MENU_STANDARD_BUTTON, buf );
	Menu_SetElement ( menu, 6, MENU_STANDARD_BUTTON, "Retour" );

	while ( (buttonClicked = Menu_TypicMenuLoop ( menu, bg, SDL_GetVideoSurface (), 60 )) != 6 )	{
		Menu_ChangeElement ( menu, buttonClicked, MENU_STANDARD_BUTTON, "Touche..." );
		Menu_DrawMenu ( menu, SDL_GetVideoSurface () );
		do	{
			SDL_WaitEvent ( &event );
		}	while ( event.type != SDL_KEYDOWN );
		switch ( buttonClicked )
		{
		case 0:
			Sys_SaveIntInFile ( "./options.ini", "Key_left", event.key.keysym.sym );
			sprintf ( buf, "Gauche: %d", event.key.keysym.sym );
			break;
		case 1:
			Sys_SaveIntInFile ( "./options.ini", "Key_right", event.key.keysym.sym );
			sprintf ( buf, "Droite: %d", event.key.keysym.sym );
			break;
		case 2:
			Sys_SaveIntInFile ( "./options.ini", "Key_drop", event.key.keysym.sym );
			sprintf ( buf, "Drop: %d", event.key.keysym.sym );
			break;
		case 3:
			Sys_SaveIntInFile ( "./options.ini", "Key_rotation", event.key.keysym.sym );
			sprintf ( buf, "Rotation: %d", event.key.keysym.sym );
			break;
		case 4:
			Sys_SaveIntInFile ( "./options.ini", "Key_pause", event.key.keysym.sym );
			sprintf ( buf, "Pause: %d", event.key.keysym.sym );
			break;
		}
		Menu_ChangeElement ( menu, buttonClicked, MENU_STANDARD_BUTTON, buf );
	}

	Menu_DeleteMenu ( menu );
	SDL_FreeSurface ( bg );
}
int  Game_Menu_Pause ( SDL_Surface *currentScreen )
{
	SDL_Surface *background = NULL,
		*mask = IMG_Load ( "./pictures/pause-mask.png" );
	Menu *pauseMenu = NULL;
	int toReturn = 0;

	if ( !mask )	{
		printf ( "Game_Menu_Pause ( SDL_Surface * ): unable to load \"./pictures/pause-mask.png\".\n" );
		return 2;
	}

	background = SDL_CreateRGBSurface ( SDL_HWSURFACE, currentScreen->w, currentScreen->h, 32, MASQUE_ROUGE, MASQUE_VERT, MASQUE_BLEU, MASQUE_ALPHA );

	SDL_BlitSurface ( currentScreen, NULL, background, NULL );
	SDL_BlitSurface ( mask, NULL, background, NULL );

	pauseMenu = Menu_CreateMenu ( MENU_W, MENU_H - 30, MENU_X, MENU_Y, 4 );
	Menu_SetElement ( pauseMenu, 0, MENU_STANDARD_BUTTON, "Reprendre" );
	Menu_SetElement ( pauseMenu, 1, MENU_STANDARD_BUTTON, "Recommencer" );
	Menu_SetElement ( pauseMenu, 3, MENU_STANDARD_BUTTON, "Quitter" );

	switch ( Menu_TypicMenuLoop ( pauseMenu, background, SDL_GetVideoSurface (), 60 ) )
	{
	case 0:
		toReturn = 2;
		break;
	case 1:
		toReturn = 1;
		break;
	case 3:
		toReturn = 0;
		break;
	default:
		toReturn = 2;
		break;
	}

	SDL_FreeSurface ( background );
	SDL_FreeSurface ( mask );
	Menu_DeleteMenu ( pauseMenu );

	return toReturn;
}
/* Options load/save w/ file management functions */
void Game_SaveOptions ( int musicEnabled, int soundEnabled )
{
	FILE *fOptions = NULL;

	fOptions = fopen ( "./options.ini", "w" );
	if ( !fOptions )	{
		printf ( "Unable to open file \"options.ini\", maybe file is missing or protected.\n" );
		return;
	}

	fprintf ( fOptions, "EnableGameMusic: %s\n", (musicEnabled) ? "no " : "yes" );
	fprintf ( fOptions, "EnableGameSounds: %s\n", (soundEnabled) ? "no " : "yes" );

	fclose ( fOptions );
}
void Game_SaveGameOption ( char *name, int value )
{
	char buf[200] = "",
		*label = NULL;
	FILE *fOptions = NULL;

	if ( !(fOptions = fopen ( "./options.ini", "rb+" )) )	{
		fprintf ( stdout, "Unable to open file \"./options.ini\", make sure the file is not locked.\n" );
		return;
	}
	if ( name[strlen(name) - 1] != ':' )	{
		label = (char*)malloc ( strlen ( name ) + 5 );
		sprintf ( label, "%s:\0", name );
	}
	else	label = name;

	while ( fgets ( buf, 190, fOptions ) )	{
		if ( strstr ( buf, label ) )	{
			fseek ( fOptions, -7, SEEK_CUR );
			while ( fgetc ( fOptions ) != ' ' );
			fseek ( fOptions, 0, SEEK_CUR );
			fprintf ( fOptions, "%s", (value == OPTION_YES) ? "yes" : "no " );
			
			fseek ( fOptions, 0, SEEK_END );

			fclose ( fOptions );
			if ( label )
				free ( label );
			return;
		}
	}
	
	fseek ( fOptions, 0, SEEK_CUR );

	/* Label seeking get to the end of the file, will create the label */
	fprintf ( fOptions, "%s: %s\n\r", name, (value == OPTION_YES) ? "yes" : "no " );

	fclose ( fOptions );
	if ( label )
		free ( label );
}
int  Game_GetGameOption ( char *name )
{
	char buf[200] = "",
		*label = NULL;
	FILE *fOptions = NULL;

	if ( !(fOptions = fopen ( "./options.ini", "rb" )) )	{
		fprintf ( stderr, "Unable to load file \"./options.ini\", file is missing or locked.\n" );
		return OPTION_YES;
	}
	if ( name[strlen(name) - 1] != ':' )	{
		label = (char*)malloc ( strlen ( name ) + 5 );
		sprintf ( label, "%s:\0", name );
	}
	else	label = name;

	while ( fgets ( buf, 190, fOptions ) )	{
		if ( strstr ( buf, label ) )	{
			fseek ( fOptions, -7, SEEK_CUR );
			while ( fgetc ( fOptions ) != ' ' );
			switch  ( fgetc ( fOptions ) )
			{
			case 'y':
				fclose ( fOptions );
				return OPTION_YES;
				break;
			case 'n':
				fclose ( fOptions );
				return OPTION_NO;
				break;

			default:
				fclose ( fOptions );
				printf ( "Options file corrupted. Option \"%s\" invalid\n", name );
				return OPTION_YES;
				break;
			}
		}
	}

	fclose ( fOptions );

	printf ( "Option \"%s\" doesn't exist.\n", name );
	return OPTION_YES;
}

int  Game_GetIntOption ( char *name )
{
	return Sys_GetIntValueInFile ( "./options.ini", name );
}
/* Play-relaed funcions */
int  Game_PlayGame ( int difficulty )
{
	SDL_Surface *ecran = SDL_GetVideoSurface (),
		**blocks = NULL,
		*gameBackground = NULL,
		*gameBackgroundVRAM = NULL;
	SDL_Event event;
	int done = 0,
		gameBoard[NB_BLOCK_W][NB_BLOCK_H] = {{0}},
		currentPattern = Game_GetNextPattern (), nextPattern = Game_GetNextPattern (), currentRotation = 0, canMoveDown = 1, speed = 1000;
	int i = 0, j = 0, replay = 0, patternX = NB_BLOCK_W / 2 - 1, patternY = 0, downKeyPressed = 0, isAtbottom = 0,
		newTime = 0, oldTimes[3] = {0}, count = 0, score = 0, linesComplete = 0, totalLinesCount = 0, tetrisDone = 0,
		playSounds = !Game_GetGameOption ( "EnableGameSounds" ),
		displayGhost = !Game_GetGameOption ( "EnableGhost" ),
		enableHiPerf = !Game_GetGameOption ( "EnableHiPerf" ),
		explodePlaying = 0, keys[5] = {0};
	SDL_Rect pBackground = {0, 0, 0, 0};

	/* 3 old times: FPS-limiter, FPS counter, gameMechanics */

	speed = 1000 - difficulty * 350;

	blocks = Game_LoadBlocksSurfaces ();
	gameBackground = Game_ConvertToVRAM ( IMG_Load ( "./pictures/background-in-game.png" ), 1 );
	for ( i = 0; i < NB_BLOCK_W; i++ )	{
		for ( j = 0; j < NB_BLOCK_H; j++ )	{
			gameBoard[i][j] = -1;
		}
	}


	keys[MOVE_DROP] = Game_GetIntOption ( "Key_drop" );
	keys[MOVE_LEFT] = Game_GetIntOption ( "Key_left" );
	keys[MOVE_PAUSE] = Game_GetIntOption ( "Key_pause" );
	keys[MOVE_RIGHT] = Game_GetIntOption ( "Key_right" );
	keys[MOVE_ROTATION] = Game_GetIntOption ( "Key_rotation" );

	for ( i = 0; i < 3; i++ )
		oldTimes[i] = SDL_GetTicks ();

	SDL_EnableKeyRepeat ( 50, 50 );

	while ( !done )	{
		newTime = SDL_GetTicks ();
		while ( SDL_PollEvent ( &event ) )	{
			switch ( event.type )
			{
			case SDL_QUIT:
				replay = 2;	// Code 2 is to exit game
				done = 1;
				break;
			case SDL_KEYDOWN:
				if ( event.key.keysym.sym == keys[MOVE_PAUSE] )	{
					if ( (replay = Game_Menu_Pause ( ecran )) != 2 )
					done = 1;
				}
				else if ( event.key.keysym.sym == keys[MOVE_ROTATION] )	{
					if ( Game_CanPatternMove ( currentPattern, currentRotation, patternX, patternY, gameBoard, DOWN ) )
						isAtbottom = 0;
					else
						isAtbottom = 1;
					currentRotation++;
					if ( currentRotation > 3 )
						currentRotation = 0;
					while ( !Game_CanPatternMove ( currentPattern, currentRotation, patternX, patternY, gameBoard, DOWN ) && !isAtbottom )
						patternX--;
					while ( !Game_CanPatternMove ( currentPattern, currentRotation, patternX, patternY, gameBoard, DOWN ) && isAtbottom )
						patternY--;
				}
				else if ( event.key.keysym.sym == SDLK_SPACE )	{
					currentPattern++;
					if ( currentPattern > 6 )
						currentPattern = 0;
				}
				else if ( event.key.keysym.sym == keys[MOVE_LEFT] )	{
					if ( Game_CanPatternMove ( currentPattern, currentRotation, patternX, patternY, gameBoard, LEFT ) )
						patternX--;
				}
				else if ( event.key.keysym.sym == keys[MOVE_RIGHT] )	{
					if ( Game_CanPatternMove ( currentPattern, currentRotation, patternX, patternY, gameBoard, RIGHT ) )
						patternX++;
				}
				else if ( event.key.keysym.sym == keys[MOVE_DROP] )
					downKeyPressed = 1;
			}
		}

		// Game mechanics
		canMoveDown = Game_CanPatternMove ( currentPattern, currentRotation, patternX, patternY, gameBoard, DOWN );
		if ( !downKeyPressed && !canMoveDown )
			downKeyPressed = 1;
		if ( downKeyPressed && canMoveDown )	{
			patternY++;
			score += 5;
		}
		if ( downKeyPressed && !canMoveDown )	{
			downKeyPressed = 0;
			Game_WritePatternToGameBoard ( currentPattern, currentRotation, patternX, patternY, gameBoard );
			linesComplete = Game_ClearCompletedLines ( gameBoard );
			speed -= linesComplete * (10 + difficulty * 15);
			if ( speed < 210 )	speed = 210;
			patternX = NB_BLOCK_W / 2 - 1;		patternY = 0;
			currentPattern = nextPattern;
			nextPattern = Game_GetNextPattern ();
			totalLinesCount += linesComplete;
			score += linesComplete * 1000;
			if ( linesComplete == 4 )	{
				score += 3000;
				tetrisDone = 1;
			}
			if ( linesComplete > 0 && playSounds )	{
				Mix_PlayChannel ( SOUND_EXPLODE_BEGIN, Game_GetSoundExplode (), linesComplete - 1 );
				explodePlaying = 1;
			}
			if ( playSounds )	{
				Mix_PlayChannel ( SOUND_BLOCK_PLACED, Game_GetSoundPlaced (), 0 );
				if ( tetrisDone )
					Mix_PlayChannel ( SOUND_TETRIS, Game_GetSoundTetris (), 0 );
			}
		}
		if ( !done )
			done = Game_CheckForLoss ( gameBoard );
		
		if ( explodePlaying && !Mix_Playing ( SOUND_EXPLODE_BEGIN ) && playSounds )	{
			Mix_PlayChannel ( SOUND_EXPLODE_END, Game_GetSoundExplodeEnd (), 0 );
			explodePlaying = 0;
		}

		/* FPS Counter */
		if ( newTime - oldTimes[1] >= 500 )	{
			printf ( "FPS: %d, speed %d, frame length %d      \r", count * 2, speed, newTime - oldTimes[0] );
			count = 0;
			oldTimes[1] = newTime;
		}
		count++;
		/* Pattern auto-mover */
		if ( newTime - oldTimes[2] >= speed )	{
			if ( !downKeyPressed && canMoveDown )
				patternY++;
			oldTimes[2] = newTime;
		}

		// Display Part
		SDL_FillRect ( ecran, NULL, SDL_MapRGB ( ecran->format, 255, 255, 255 ) );
		SDL_BlitSurface ( gameBackground, NULL, ecran, &pBackground );
		if ( displayGhost )
			Game_DisplayGhostPattern ( patternX, patternY, currentPattern, currentRotation, gameBoard );
		Game_DisplayGameBoard ( ecran, gameBoard );
		Game_DrawPatterns ( currentPattern, currentRotation, patternX, patternY );
		Game_DisplayInfoZone ( score, totalLinesCount, difficulty );
		Game_DisplayNextPattern ( nextPattern );
		if ( tetrisDone )
			tetrisDone = Game_DisplayTetris ( newTime );

		SDL_Flip ( ecran );
		//SDL_Delay ( (10 - (newTime - oldTimes[0]) >= 0) ? 10 - (newTime - oldTimes[0]) : 0 );
		//SDL_Delay ( 15 );
		//SDL_Delay ( 10 - (newTime - oldTimes[0] - 10) );
		if ( !enableHiPerf )
			SDL_Delay ( (10 - (newTime - oldTimes[0] - 10) >= 0) ? 10 - (newTime - oldTimes[0] - 10) : 0 );
		oldTimes[0] = newTime;
	}

	SDL_FreeSurface ( gameBackground );
	// Game_FreeBlocksSurface ( blocks );


	return replay;
}

int  Game_CheckForLines ( int gameBoard[NB_BLOCK_W][NB_BLOCK_H] )
{
	// Returns the amount of completed lines
	int linesCleared = 0, x = 0, y = 0, lineCompleted = 1;

	for ( y = 0; y < NB_BLOCK_H; y++ )	{
		lineCompleted = 1;
		for ( x = 0; x < NB_BLOCK_H; x++ )	{
			if ( gameBoard[x][y] == -1 )
				lineCompleted = 0;
		}
		if ( lineCompleted )
			linesCleared++;
	}

	return linesCleared;
}
int  Game_CanPatternMove ( int pattern, int rotation, int x, int y, int gameBoard[NB_BLOCK_W][NB_BLOCK_H], Game_Directions direction )
{
	int deltaX = 0, deltaY = 0, i, j;
	switch ( direction )
	{
	case DOWN:
		deltaY = 1;
		break;
	case LEFT:
		deltaX = -1;
		break;
	case RIGHT:
		deltaX = 1;
		break;
	default:
		deltaY = 1;
		break;
	}
	
	for ( i = 0; i < 4; i++ )	{
		for ( j = 0; j < 4; j++ )	{
			if ( x + deltaX + i >= NB_BLOCK_W || y + deltaY + j >= NB_BLOCK_H ||
				 x + deltaX + i < 0 || y + deltaY + j < 0 )	{
				if ( Game_GetPatternValue ( pattern, rotation, i, j ) != -1 )
					return 0;
				else
					continue;
			}
			if ( Game_GetPatternValue ( pattern, rotation, i, j ) != -1 && gameBoard[x + deltaX + i][y + deltaY + j] != -1 )
				return 0;
		}
	}

	return 1;
}
void Game_WritePatternToGameBoard ( int pattern, int rotation, int x, int y, int gameBoard[NB_BLOCK_W][NB_BLOCK_H] )
{
	int i, j, value;

	for ( i = 0; i < 4; i++ )	{
		for ( j = 0; j < 4; j++ )	{
			if ( (value = Game_GetPatternValue ( pattern, rotation, i, j )) != -1 )
				gameBoard[x + i][y + j] = value;
		}
	}
}

int	 Game_GetPatternValue ( int pattern, int rotation, int x, int y )
{
	static int patterns[7][4][4][4] = {
		{
			{
				{BLOCK_RED, -1, -1, -1},
				{BLOCK_RED, BLOCK_RED, -1, -1},
				{-1, BLOCK_RED, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{-1, BLOCK_RED, BLOCK_RED, -1},
				{BLOCK_RED, BLOCK_RED, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_RED, -1, -1, -1},
				{BLOCK_RED, BLOCK_RED, -1, -1},
				{-1, BLOCK_RED, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{-1, BLOCK_RED, BLOCK_RED, -1},
				{BLOCK_RED, BLOCK_RED, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			}
		},
		{
			{
				{-1, BLOCK_GREEN, -1, -1},
				{BLOCK_GREEN, BLOCK_GREEN, -1, -1},
				{BLOCK_GREEN, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_GREEN, BLOCK_GREEN, -1, -1},
				{-1, BLOCK_GREEN, BLOCK_GREEN, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{-1, BLOCK_GREEN, -1, -1},
				{BLOCK_GREEN, BLOCK_GREEN, -1, -1},
				{BLOCK_GREEN, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_GREEN, BLOCK_GREEN, -1, -1},
				{-1, BLOCK_GREEN, BLOCK_GREEN, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			}
		},
		{
			{
				{BLOCK_BLUE, BLOCK_BLUE, -1, -1},
				{-1, BLOCK_BLUE, -1, -1},
				{-1, BLOCK_BLUE, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{-1, -1, BLOCK_BLUE, -1},
				{BLOCK_BLUE, BLOCK_BLUE, BLOCK_BLUE, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_BLUE, -1, -1, -1},
				{BLOCK_BLUE, -1, -1, -1},
				{BLOCK_BLUE, BLOCK_BLUE, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_BLUE, BLOCK_BLUE, BLOCK_BLUE, -1},
				{BLOCK_BLUE, -1, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			}
		},
		{
			{
				{BLOCK_YELLOW, BLOCK_YELLOW, -1, -1},
				{BLOCK_YELLOW, BLOCK_YELLOW, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_YELLOW, BLOCK_YELLOW, -1, -1},
				{BLOCK_YELLOW, BLOCK_YELLOW, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_YELLOW, BLOCK_YELLOW, -1, -1},
				{BLOCK_YELLOW, BLOCK_YELLOW, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_YELLOW, BLOCK_YELLOW, -1, -1},
				{BLOCK_YELLOW, BLOCK_YELLOW, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			}
		},
		{
			{
				{BLOCK_ORANGE, BLOCK_ORANGE, -1, -1},
				{BLOCK_ORANGE, -1, -1, -1},
				{BLOCK_ORANGE, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_ORANGE, BLOCK_ORANGE, BLOCK_ORANGE, -1},
				{-1,		   -1,			 BLOCK_ORANGE, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{-1, BLOCK_ORANGE, -1, -1},
				{-1, BLOCK_ORANGE, -1, -1},
				{BLOCK_ORANGE, BLOCK_ORANGE, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_ORANGE, -1, -1, -1},
				{BLOCK_ORANGE, BLOCK_ORANGE, BLOCK_ORANGE, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			}
		},
		{
			{
				{BLOCK_VIOLET, -1, -1, -1},
				{BLOCK_VIOLET, BLOCK_VIOLET, -1, -1},
				{BLOCK_VIOLET, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_VIOLET, BLOCK_VIOLET, BLOCK_VIOLET, -1},
				{-1, BLOCK_VIOLET, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{-1, BLOCK_VIOLET, -1, -1},
				{BLOCK_VIOLET, BLOCK_VIOLET, -1, -1},
				{-1, BLOCK_VIOLET, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{-1, BLOCK_VIOLET, -1, -1},
				{BLOCK_VIOLET, BLOCK_VIOLET, BLOCK_VIOLET, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			}
		},
		{
			{
				{BLOCK_CYAN, -1, -1, -1},
				{BLOCK_CYAN, -1, -1, -1},
				{BLOCK_CYAN, -1, -1, -1},
				{BLOCK_CYAN, -1, -1, -1}
			},
			{
				{BLOCK_CYAN, BLOCK_CYAN, BLOCK_CYAN, BLOCK_CYAN},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			},
			{
				{BLOCK_CYAN, -1, -1, -1},
				{BLOCK_CYAN, -1, -1, -1},
				{BLOCK_CYAN, -1, -1, -1},
				{BLOCK_CYAN, -1, -1, -1}
			},
			{
				{BLOCK_CYAN, BLOCK_CYAN, BLOCK_CYAN, BLOCK_CYAN},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1},
				{-1, -1, -1, -1}
			}
		}
	};


	return patterns[pattern][rotation][x][y];
}

int  Game_ClearCompletedLines ( int gameBoard[NB_BLOCK_W][NB_BLOCK_H] )
{
	int x = 0, y = 0, linesAmount = Game_CheckForLines ( gameBoard ), i = 0, lineComplete = 1;

	linesAmount = Game_CheckForLines ( gameBoard );

	while ( Game_CheckForLines ( gameBoard ) ) 	{	// Each pass corresponds to one line
		for ( y = 0; y < NB_BLOCK_H; y++ )	{
			lineComplete = 1;
			for ( x = 0; x < NB_BLOCK_W; x++ )	{
				if ( gameBoard[x][y] == -1 )
					lineComplete = 0;
			}
			if ( lineComplete )	{
				for ( i = y; i > 0; i-- )	{
					for ( x = 0; x < NB_BLOCK_W; x++ )	{
						gameBoard[x][i] = gameBoard[x][i - 1];
						gameBoard[x][i - 1] = -1;
					}
				}
			}
		}
	}

	return linesAmount;
}
int  Game_GetNextPattern ()
{
	return rand () % 7;
}
int  Game_CheckForLoss ( int gameBoard[NB_BLOCK_W][NB_BLOCK_H] )
{
	int x = 0;
	for ( x = 0; x < NB_BLOCK_W; x++ )	{
		if ( gameBoard[x][0] != -1 )
			return 1;
	}
	return 0;
}
/* Displaying functions */
void Game_DrawPatternsOn ( int pattern, int rotation, double x, double y, SDL_Surface *dst )
{
	static SDL_Surface **blocks = Game_LoadBlocksSurfaces ();
	SDL_Rect pBlock = {0};
	int i, j;

	if ( !blocks )	return;
	while ( pattern >= 7 )	pattern -= 7;
	while ( rotation >= 4 )	rotation -= 4;

	/************************************************************
	 * This pattern var is ... special, made of four arrays:	*
	 * patterns[pattern][rotation][y][x]						*
	 ************************************************************/

	for ( i = 0; i < 4; i++ )	{
		for ( j = 0; j < 4; j++ )	{
			if ( Game_GetPatternValue ( pattern, rotation, i, j ) != -1 )	{
				pBlock.x = BORDER_SIZE + BLOCK_SIZE * x + BLOCK_SIZE * i;
				pBlock.y = BORDER_SIZE + BLOCK_SIZE * y + BLOCK_SIZE * j;

				SDL_BlitSurface ( blocks[pattern], NULL, dst, &pBlock );
			}
		}
	}
}
void Game_DrawPatterns ( int pattern, int rotation, double x, double y )	{
	Game_DrawPatternsOn ( pattern, rotation, x, y, SDL_GetVideoSurface () );
}
void Game_DisplayGameBoard ( SDL_Surface *dst, int gameBoard[NB_BLOCK_W][NB_BLOCK_H] )
{
	SDL_Rect pBoard = {};
	static SDL_Surface **blocks = NULL;
	int x = 0, y = 0;

	if ( !blocks )
		blocks = Game_LoadBlocksSurfaces ();
	if ( !blocks )
		return;

	pBoard.x = BORDER_SIZE;
	pBoard.y = BORDER_SIZE;

	for ( x = 0; x < NB_BLOCK_W; x++ )	{
		pBoard.y = BORDER_SIZE;
		for ( y = 0; y < NB_BLOCK_H; y++ )	{
			if ( gameBoard[x][y] != -1 )
				SDL_BlitSurface ( blocks[gameBoard[x][y]], NULL, dst, &pBoard );
			pBoard.y += BLOCK_SIZE;
		}
		pBoard.x += BLOCK_SIZE;
	}
}
void Game_DisplayInfoZone ( int score, int linesCompleted, int difficulty )
{
	static TTF_Font *computerfont = Game_GetComputerfont (),
		*courierNew = Game_GetCouriernew ();
	static int previousScore = score, previousLinesCompleted = linesCompleted, previousDifficulty = difficulty;
	static SDL_Surface *surfaceScore = NULL, *surfaceLinesCompleted = NULL, *surfaceDifficulty = NULL,
		*surfaceScoreLabel = NULL, *surfaceLinesCompletedLabel = NULL, *surfaceDifficultyLabel = NULL,
		*ecran = SDL_GetVideoSurface ();
	static char buf[50] = "";
	SDL_Color black = {0, 0, 0, 0};
	SDL_Rect positions = {0};

	if ( !computerfont || !courierNew )	return;

	/* Re-render the surface */
	if ( !surfaceScore || score != previousScore )	{
		sprintf ( buf, "%d\0", score );
		if ( surfaceScore )
			SDL_FreeSurface ( surfaceScore );
		surfaceScore = Game_ConvertToVRAM ( TTF_RenderText_Blended ( computerfont, buf, black ), 1 );
		previousScore = score;
	}

	if ( !surfaceLinesCompleted || linesCompleted != previousLinesCompleted )	{
		sprintf ( buf, "%d\0", linesCompleted );
		if ( surfaceLinesCompleted )
			SDL_FreeSurface ( surfaceLinesCompleted );
		surfaceLinesCompleted = Game_ConvertToVRAM ( TTF_RenderText_Blended ( computerfont, buf, black ), 1 );
		previousLinesCompleted = linesCompleted;
	}

	if ( !surfaceDifficulty || difficulty != previousDifficulty )	{
		if ( surfaceDifficulty )
			SDL_FreeSurface ( surfaceDifficulty );
		switch ( difficulty )
		{
		case 0:
			strcpy ( buf, "Facile" );
			break;
		case 1:
			strcpy ( buf, "Moyen" );
			break;
		case 2:
			strcpy ( buf, "Difficile" );
			break;
		default:
			strcpy ( buf, "BUG SPOTTED!" );
			break;
		}
		surfaceDifficulty = Game_ConvertToVRAM ( TTF_RenderText_Blended ( computerfont, buf, black ), 1 );
		previousDifficulty = difficulty;
	}

	if ( !surfaceScoreLabel )
		surfaceScoreLabel = Game_ConvertToVRAM ( TTF_RenderText_Blended ( courierNew, "Score", black ), 1 );
	if ( !surfaceLinesCompletedLabel )
		surfaceLinesCompletedLabel = Game_ConvertToVRAM ( TTF_RenderText_Blended ( courierNew, "Lignes", black ), 1 );
	if ( !surfaceDifficultyLabel )
		surfaceDifficultyLabel = Game_ConvertToVRAM ( TTF_RenderText_Blended ( courierNew, "Difficult�", black ), 1 );

	positions.x = 2 * BORDER_SIZE + NB_BLOCK_W * BLOCK_SIZE + BORDER_SIZE / 2;
	positions.y = 3 * BORDER_SIZE + 5 * BLOCK_SIZE;
	SDL_BlitSurface ( surfaceScoreLabel, NULL, ecran, &positions );
	positions.y += surfaceScoreLabel->h;
	SDL_BlitSurface ( surfaceScore, NULL, ecran, &positions );
	positions.y += (surfaceScore->h + BORDER_SIZE);
	SDL_BlitSurface ( surfaceLinesCompletedLabel, NULL, ecran, &positions );
	positions.y += surfaceLinesCompletedLabel->h;
	SDL_BlitSurface ( surfaceLinesCompleted, NULL, ecran, &positions );
	positions.y += (surfaceLinesCompleted->h + BORDER_SIZE);
	SDL_BlitSurface ( surfaceDifficultyLabel, NULL, ecran, &positions );
	positions.y += surfaceDifficultyLabel->h;
	SDL_BlitSurface ( surfaceDifficulty, NULL, ecran, &positions );
}

void Game_DisplayNextPattern ( int pattern )
{
	double x = NB_BLOCK_W + 1, y = 0;
	switch ( pattern )
	{
		/* 2-wide, 2-height patterns */
	case BLOCK_YELLOW:
		x += 1.5;
		y += 1.5;
		break;
		/* 3-wide, 2-height patterns */
	case BLOCK_RED:
	case BLOCK_GREEN:
	case BLOCK_BLUE:
	case BLOCK_ORANGE:
	case BLOCK_VIOLET:
		x += 1;
		y += 1.5;
		break;
		/* 4-wide, 1-height patterns */
	case BLOCK_CYAN:
		x += 0.5;
		y = 2;
		break;
	}

	Game_DrawPatterns ( pattern, 0, x, y );
}
int  Game_DisplayTetris ( int time )
{
	static int originTime = 0, tetrisDisplaying = 1;
	static SDL_Surface *tetrisText = NULL,
		*bg = NULL,
		*ecran = SDL_GetVideoSurface ();
	SDL_Color black = {0, 0, 0, 0};
	SDL_Rect pText = {0};

	if ( time == -1 )	{	/* signal for freeing memory */
		SDL_FreeSurface ( tetrisText );
		SDL_FreeSurface ( bg );
		return -1;
	}

	if ( !tetrisText && Game_GetBleedingcowboys () )
		tetrisText = Game_ConvertToVRAM ( TTF_RenderText_Blended ( Game_GetBleedingcowboys (), "TETRIS !", black ), 1 );

	pText.x = BORDER_SIZE + NB_BLOCK_W * BLOCK_SIZE / 2 - tetrisText->w / 2;	pText.y = ecran->h / 2 - tetrisText->h / 2;
	pText.h = tetrisText->h;		pText.w = tetrisText->w;

	if ( !bg )	bg = SDL_CreateRGBSurface ( SDL_HWSURFACE, tetrisText->w, tetrisText->h, 32, 0, 0, 0, 0 );
	if ( bg )	{
		SDL_BlitSurface ( ecran, &pText, bg, NULL );
		SDL_BlitSurface ( tetrisText, NULL, bg, NULL );
	}

	SDL_SetAlpha ( bg, SDL_SRCALPHA, SDL_ALPHA_OPAQUE );

	if ( originTime == 0 )	originTime = time;
	if ( time - originTime > 2000 )	{
		originTime = time;
		if ( tetrisDisplaying )
			tetrisDisplaying = 0;
		else
			tetrisDisplaying = 1;
	}

	if ( time - originTime > 0 && time - originTime < 1000 )	tetrisDisplaying = 1;

	if ( time - originTime >= 1000 && time - originTime < 1500 )	{
		//SDL_MySetAlpha ( tetrisText, 255 - (int)((double)(time - originTime - 1000) / 500.0 * 255) );
		SDL_SetAlpha ( bg, SDL_SRCALPHA, 255 - (int)((double)(time - originTime - 1000) / 500.0 * 255) );
		//printf ( "Alpha: %d\t", 255 - (int)((double)(time - originTime - 1000) / 500.0 * 255) );
	}

	if ( time - originTime >= 1500 )
		//SDL_MySetAlpha ( tetrisText, SDL_ALPHA_TRANSPARENT );
		SDL_SetAlpha ( bg, SDL_SRCALPHA, SDL_ALPHA_TRANSPARENT );

	if ( tetrisDisplaying )
		SDL_BlitSurface ( bg, NULL, ecran, &pText );

	return tetrisDisplaying;
}
void Game_DisplayGhostPattern ( int patternX, int patternY, int pattern, int rotation, int gameBoard[NB_BLOCK_W][NB_BLOCK_H] )
{
	static int y = 0;
	static int prevPatternX = 0, prevPattern = 0, prevRotation = 0;
	static SDL_Surface *dst = NULL;
	static SDL_Rect pBlock = {0};

	if ( !dst )
		dst = SDL_CreateRGBSurface ( SDL_HWSURFACE, BLOCK_SIZE * 4, BLOCK_SIZE * 4, 32, 0, 0, 0, 0 );

	if ( patternX != prevPatternX || pattern != prevPattern || rotation != prevRotation )	{
		prevPatternX = patternX;
		prevPattern = pattern;
		prevRotation = rotation;
		y = patternY;
		pBlock.h = BLOCK_SIZE * 4;	pBlock.w = BLOCK_SIZE * 4;

		while ( Game_CanPatternMove ( pattern, rotation, patternX, y, gameBoard, DOWN ) ) y++;

		pBlock.x = BORDER_SIZE + patternX * BLOCK_SIZE;	pBlock.y = BORDER_SIZE + y * BLOCK_SIZE;

		SDL_SetAlpha ( dst, SDL_SRCALPHA, 255 );
		SDL_BlitSurface ( SDL_GetVideoSurface (), &pBlock, dst, NULL );
		//SDL_FillRect ( dst, NULL, SDL_MapRGB ( dst->format, 255, 255, 255 ));
	}
	
	Game_DrawPatternsOn ( pattern, rotation, -1, -1, dst );
	SDL_SetAlpha ( dst, SDL_SRCALPHA, 128 );
	SDL_BlitSurface ( dst, NULL, SDL_GetVideoSurface (), &pBlock );
}
/* Resources management */
SDL_Surface* Game_ConvertToVRAM ( SDL_Surface *src, int freeSurface )
{
	/*SDL_Surface *buf = NULL;

	if ( src->flags & SDL_HWSURFACE )		return src;

	if ( !(buf = SDL_CreateRGBSurface ( SDL_HWSURFACE | SDL_SRCALPHA, src->w, src->h, 32, 0, 0, 0, 0 )) )	{
		printf ( "SDL_Surface* Game_ConvertToVRAM (SDL_Surface*, int): unable to create a %dx%dx%d surface in VRAM.\n", src->w, src->h, 32 );
		return src;
	}

	SDL_BlitSurface ( src, NULL, buf, NULL );
	if ( freeSurface )	SDL_FreeSurface ( src );

	return buf;*/

	return SDL_DisplayFormatAlpha ( src );
}
SDL_Surface** Game_LoadBlocksSurfaces ()
{
	static SDL_Surface **buffer = NULL;
	int i = 0;
	
	if ( buffer )
		return buffer;

	buffer = (SDL_Surface**)malloc ( sizeof(SDL_Surface*) * 7 );
	if ( !buffer )	{
		printf ( "Game_LoadBlocksSurfaces: Unable to allocate %d bytes of memory.\n", sizeof(SDL_Surface*) * 7 );
		return NULL;
	}

	buffer[BLOCK_BLUE] = Game_ConvertToVRAM ( IMG_Load ( "./pictures/blocks/blockBlue.png" ), 1 );
	buffer[BLOCK_CYAN] = Game_ConvertToVRAM ( IMG_Load ( "./pictures/blocks/blockCyan.png" ), 1 );
	buffer[BLOCK_GREEN] = Game_ConvertToVRAM ( IMG_Load ( "./pictures/blocks/blockGreen.png" ), 1 );
	buffer[BLOCK_ORANGE] = Game_ConvertToVRAM ( IMG_Load ( "./pictures/blocks/blockOrange.png" ), 1 );
	buffer[BLOCK_RED] = Game_ConvertToVRAM ( IMG_Load ( "./pictures/blocks/blockRed.png" ), 1 );
	buffer[BLOCK_VIOLET] = Game_ConvertToVRAM ( IMG_Load ( "./pictures/blocks/blockViolet.png" ), 1 );
	buffer[BLOCK_YELLOW] = Game_ConvertToVRAM ( IMG_Load ( "./pictures/blocks/blockYellow.png" ), 1 );

	for ( i = 0; i < 7; i++ )	{
		if ( buffer[i] == NULL )
			i = 10;
		if ( buffer[i]->flags & SDL_HWSURFACE )		printf ( "Block %d is in VRAM.\n", i );
		else	printf ( "Block %d is in RAM.\n", i );
	}
	if ( i == 10 )	{
		for ( i = 0; i < 7; i++ )
			SDL_FreeSurface ( buffer[i] );
		free ( buffer );
		buffer = NULL;
		printf ( "Game_LoadBlocksSurfaces: Unable to load blocks pictures. Make sure none is missing.\n" );
	}

	return buffer;
}
TTF_Font* Game_GetComputerfont ()
{
	static TTF_Font *computerfont = NULL;

	if ( !computerfont )
		computerfont = TTF_OpenFont ( "./fonts/computerfont.ttf", 14 );
	if ( !computerfont )
		printf ( "Game_GetComputerfont (): unable to load font \"./fonts/computerfont.ttf\".\n" );
	
	return computerfont;
}

TTF_Font* Game_GetCouriernew ()
{
	static TTF_Font *couriernew = NULL;

	if ( !couriernew )
		couriernew = TTF_OpenFont ( "./fonts/couriernewb.ttf", 14 );
	if ( !couriernew )
		printf ( "Game_GetCoouriernew (): unable to load font \"./fonts/couriernewb.ttf\".\n" );

	return couriernew;
}
TTF_Font* Game_GetBleedingcowboys ()
{
	static TTF_Font *bleedingcowboys = NULL;

	if ( bleedingcowboys )	return bleedingcowboys;

	bleedingcowboys = TTF_OpenFont ( "./fonts/Bleeding_Cowboys.ttf", 50 );

	if ( !bleedingcowboys )
		printf ( "Game_GetBleedingcowboys (): unable to load font \"./fonts/Bleeding_Cowboys.ttf\".\n" );

	return bleedingcowboys;
}
Mix_Chunk* Game_GetSoundPlaced ()
{
	Mix_Chunk *sound = NULL;

	if ( sound )	return sound;

	sound = Mix_LoadWAV ( "./sound/block_placed.ogg" );

	return sound;
}
Mix_Chunk* Game_GetSoundExplode ()
{
	Mix_Chunk *sound = NULL;

	if ( sound )	return sound;

	sound = Mix_LoadWAV ( "./sound/explode_begin.ogg" );

	return sound;
}
Mix_Chunk* Game_GetSoundExplodeEnd ()
{
	Mix_Chunk *sound = NULL;

	if ( sound )	return sound;

	sound = Mix_LoadWAV ( "./sound/explode_end.ogg" );

	return sound;
}
Mix_Chunk* Game_GetSoundTetris ()
{
	Mix_Chunk *sound = NULL;

	if ( sound )	return sound;

	sound = Mix_LoadWAV ( "./sound/tetris.ogg" );

	return sound;
}
void Game_FreeBlocksSurface ( SDL_Surface **blocks )
{
	int i = 0;
	for ( i = 0; i < 7; i++ )
		SDL_FreeSurface ( blocks[i] );
}