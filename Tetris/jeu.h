/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/


#ifndef JEU_H
#define JEU_H

enum	{
	BLOCK_RED, BLOCK_GREEN, BLOCK_BLUE, BLOCK_YELLOW, BLOCK_ORANGE, BLOCK_VIOLET, BLOCK_CYAN
};

typedef enum Game_Directions	{
	DOWN, LEFT, RIGHT
};

enum	{
	MOVE_LEFT, MOVE_RIGHT, MOVE_DROP, MOVE_ROTATION, MOVE_PAUSE
};

/*************************************************************************************
 * Menus Functions																	 *
 *************************************************************************************/
/* Displays main menu */
void Game_StartGameMenus ();
/* Displays Play menu w/ difficulty choice */
void Game_Menu_Play ();
/* Displays options menu */
void Game_Menu_Options ();
/* Displays pause menu. <currentScreen> is the surface to display as background
 * usually the <screen> surface
 **/
int  Game_Menu_Pause ( SDL_Surface *currentScreen );
/* Displays key-select menu */
void Game_Menu_OptionsKeys ();

/*************************************************************************************
 * Load/Save functions																 *
 *************************************************************************************/
/* To Change, saves options in the options.ini file */
void Game_SaveOptions ( int musicEnabled, int soundEnabled );
/* Saves the option labeled <name> with the given value <value>
 * Case-sensitive, string must end with a '\0'
 **/
void Game_SaveGameOption ( char *name, int value );
/* Returns the option labeled <name> in the option.ini file
 * Case-sensitive, string must end with a '\0'
 **/
int  Game_GetIntOption ( char *name );
int  Game_GetGameOption ( char *name );
/* Returns an pointer to an array made of 7 surfaces for the 7 differents blocks colors
 * First call loads from the hard disk, next calls will return the same address without
 * reloading from the files
 **/
SDL_Surface** Game_LoadBlocksSurfaces ();
/* Will free the 7 surfaces loaded by Game_LoadBlocksSurfaces ()
 * Best use is Game_FreeBlocksSurface ( Game_LoadBlocksSurfaces () )
 **/
void Game_FreeBlocksSurface ( SDL_Surface **blocks );
/* Quite ... Special. Will return the value of the block at x,y on the pattern (aka.
 * Tetris Shape) specified at the rotation specified
 **/
int  Game_GetPatternValue ( int pattern, int rotation, int x, int y );
/* Returns a pointer to a TTF_Font structure, which is loaded once on the first call,
 * then the same address is returned each time.
 **/
TTF_Font* Game_GetComputerfont ();
TTF_Font* Game_GetCouriernew ();
TTF_Font* Game_GetBleedingcowboys ();
/* Returns a newly allocated surface in VRAM to have FAR faster blits for surfaces
 * If allocation fails in VRAM, returns src
 * If src is already in VRAM, will return src
 * If freeSurface is set to 0, th function won't free the src surface
 **/
SDL_Surface* Game_ConvertToVRAM ( SDL_Surface *src, int freeSurface );
/* Returns a pointer to a Mix_Chunk structure for sound effects playing
 * Loads the sound on the first call, then returns the same address each time
 **/
Mix_Chunk* Game_GetSoundPlaced ();
Mix_Chunk* Game_GetSoundExplode ();
Mix_Chunk* Game_GetSoundExplodeEnd ();
Mix_Chunk* Game_GetSoundTetris ();

/*************************************************************************************
 * Play-related functions															 *
 *************************************************************************************/
/* Starts the game with specified difficulty, returns:
 * 2: close the game
 * 1: replay
 * 0: return to main menu
 **/
int  Game_PlayGame ( int difficulty );
/* Draw the specified pattern at the x,y coordinates
 * Warning, x and y are not SDL's coordinates, but starting at the top left hand corner
 * of the play area, and 1 counts as 20 pixels, or a block
 **/
void Game_DrawPatterns ( int pattern, int rotation, double x, double y );
/* Draws the specified gameBoard on the specified surface, usually <screen> */
void Game_DisplayGameBoard ( SDL_Surface *dst, int gameBoard[NB_BLOCK_W][NB_BLOCK_H] );
/* Will check if some lines are completed in <gameBoard> and returns how many are completed */
int  Game_CheckForLines ( int gameBoard[NB_BLOCK_W][NB_BLOCK_H] );
/* Tells if the specified pattern can move in the given direction */
int  Game_CanPatternMove ( int pattern, int rotation, int x, int y, int gameBoard[NB_BLOCK_W][NB_BLOCK_H], Game_Directions direction );
/* Saves the given pattern to the gameBoard */
void Game_WritePatternToGameBoard ( int pattern, int rotation, int x, int y, int gameBoard[NB_BLOCK_W][NB_BLOCK_H] );
/* Removes the completed lines, moves the blocks donwards to fill them and returns how many
 * were removed.
 **/
int  Game_ClearCompletedLines ( int gameBoard[NB_BLOCK_W][NB_BLOCK_H] );
/* Returns a pattern chosen randomly, in the range [0;7[ */
int  Game_GetNextPattern ();
/* Returns 1 if the game is over for the given game board */
int  Game_CheckForLoss ( int gameBoard[NB_BLOCK_W][NB_BLOCK_H] );
/* Draws the content of the info zone, such as score, completed lines and difficulty */
void Game_DisplayInfoZone ( int score, int linesCompleted, int difficulty );
/* Displays the given pattern in the next pattern zone */
void Game_DisplayNextPattern ( int pattern );
/* Displays the "TETRIS !" text on the screen
 * Will free its memory if time = -1
 **/
int  Game_DisplayTetris ( int time );
/* If enabled, will display the place where the pattern will land
 * WARNING: can be resource-intensive on lower-end computers
 **/
void Game_DisplayGhostPattern ( int patternX, int patternY, int pattern, int rotation, int gameBoard[NB_BLOCK_W][NB_BLOCK_H] );

#endif