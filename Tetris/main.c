/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/

#include "main.h"
#include "menu.h"
#include "jeu.h"
#include "system.h"

int main ( int argc, char ** argv )
{
	Main_InitGame ();
	Main_LoadResources ();

	Game_StartGameMenus ();

	Main_UnloadResources ();

	return 0;
}

void Main_InitGame ()
{
	Mix_Music *music = NULL;

	if ( SDL_Init ( SDL_INIT_VIDEO ) == -1 )	{
		fprintf ( stderr, "Erreur d'initialisation de SDL_Init : %s\n", SDL_GetError () );
		exit ( EXIT_FAILURE );
	}
	atexit ( SDL_Quit );

	if ( TTF_Init () == -1 )	{
		fprintf ( stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError () );
		exit ( EXIT_FAILURE );
	}
	atexit ( TTF_Quit );

	if ( Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1 )	{
		fprintf ( stderr, "Erreur d'initialisation de Mix_OpenAudio: %s\n", Mix_GetError () );
		exit ( EXIT_FAILURE );
	}
	atexit ( Mix_CloseAudio );

	SDL_SetVideoMode ( BORDER_SIZE*3 + BLOCK_SIZE*NB_BLOCK_W + INFO_AERA_W,
				   BORDER_SIZE*2 + BLOCK_SIZE*NB_BLOCK_H,
				   32,
				   SDL_HWSURFACE | SDL_DOUBLEBUF );
	SDL_WM_SetCaption ( "Tetris", NULL );
	printf ( "W= %d\nH= %d\n", BORDER_SIZE*3 + BLOCK_SIZE*NB_BLOCK_W + INFO_AERA_W, BORDER_SIZE*2 + BLOCK_SIZE*NB_BLOCK_H );

	srand ( time ( NULL ));

	Mix_AllocateChannels ( SOUNDS_AMOUNT );

	if ( !(music = Mix_LoadMUS ( "./music/tetris.mid" )) )	{
		fprintf ( stderr, "Unable to open music \"./music/tetris.mid\"\n" );
	} else {
		Mix_PlayMusic ( music, -1 );
		if ( Game_GetGameOption ( "EnableGameMusic" ) )
			Mix_HaltMusic ();
	}

	Mix_Volume ( SOUND_EXPLODE_BEGIN, 64 );
	Mix_Volume ( SOUND_EXPLODE_END, 64 );
	Mix_VolumeMusic ( 64 );
}
void Main_LoadResources ()
{
	Game_GetBleedingcowboys ();
	Game_GetComputerfont ();
	Game_GetCouriernew ();
	Game_GetSoundExplode ();
	Game_GetSoundExplodeEnd ();
	Game_GetSoundPlaced ();
	Game_GetSoundTetris ();
	Game_LoadBlocksSurfaces ();
	Game_DisplayTetris ( 0 );
}
void Main_UnloadResources ()
{
	TTF_CloseFont ( Game_GetBleedingcowboys () );
	TTF_CloseFont ( Game_GetComputerfont () );
	TTF_CloseFont ( Game_GetCouriernew () );
	Mix_FreeChunk ( Game_GetSoundExplode () );
	Mix_FreeChunk ( Game_GetSoundExplodeEnd () );
	Mix_FreeChunk ( Game_GetSoundPlaced () );
	Mix_FreeChunk ( Game_GetSoundTetris () );
	Game_FreeBlocksSurface ( Game_LoadBlocksSurfaces () );
	Game_DisplayTetris ( -1 );
}