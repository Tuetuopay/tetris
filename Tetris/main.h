/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/

#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>

#include "pixel.h"

#define NB_BLOCK_H	20
#define NB_BLOCK_W 	14
#define BORDER_SIZE	20   // pixels
#define BLOCK_SIZE	20   // pixels
#define INFO_AERA_W	100  // pixels

#define MENU_H	300
#define MENU_W	380
#define MENU_X	30
#define MENU_Y	120

#define _DEBUG_MODE

#ifdef _DEBUG_MODE
#undef stderr
#define stderr stdout
#endif

#define SOUNDS_AMOUNT 5

enum	{
	SOUND_BLOCK_PLACED, SOUND_CLICK, SOUND_EXPLODE_BEGIN, SOUND_EXPLODE_END, SOUND_TETRIS
};

void Main_InitGame ();
void Main_LoadResources ();
void Main_UnloadResources ();

#endif

/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/
