/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/


#include "main.h"
#include "menu.h"

void clearSurface ( SDL_Surface *dst, Uint32 color )
{
	int x, y;
	
	SDL_LockSurface ( dst );
	for ( y = 0; y < dst->h; y++ )	{
		for ( x = 0; x < dst->w; x++ )	{
			SDL_SetPixel ( dst, x, y, color );
		}
	}
	SDL_UnlockSurface ( dst );
}

SDL_Surface* renderButton ( SDL_Surface *btnBG, char text[], TTF_Font *police, SDL_Surface *btnOnOff )
{
	SDL_Surface *btn = NULL,
		*texte = NULL;
	SDL_Rect pTemp = {0};
	SDL_Color noir = {0};
	Uint8 r, g, b, a;

	if ( !btnBG )	btnBG = IMG_Load ( "./pictures/buttons/standardButton.png" );
	
	texte = TTF_RenderText_Blended ( police, text, noir );
	btn = btnBG;
	
	if ( btnOnOff )	{ // si c'est un bouton de type on off
		pTemp.x = btn->w - btnOnOff->w;
		pTemp.y = 0;
		SDL_BlitSurface ( btnOnOff, NULL, btn, &pTemp );
		pTemp.x = (btn->w - btnOnOff->w) / 2 - texte->w / 2;
		pTemp.y = btn->h / 2 - texte->h / 2;
		SDL_BlitSurface ( texte, NULL, btn, &pTemp );
	} else {
		pTemp.x = btn->w / 2 - texte->w / 2;
		pTemp.y = btn->h / 2 - texte->h / 2;
		SDL_BlitSurface ( texte, NULL, btn, &pTemp );
	}

	return btn;
}

Menu *Menu_CreateMenu ( int width, int height, int x, int y, int elementsAmount )
{
	Menu *menu = NULL;
	int i = 0;

	menu = (Menu*)malloc ( sizeof(Menu) );

	menu->nbOfElements = elementsAmount;
	menu->elements = (ElementMenu*)malloc ( sizeof(ElementMenu) * elementsAmount );
	menu->buttonON = SDL_DisplayFormatAlpha ( IMG_Load ( "./pictures/buttons/buttonON.png" ) );
	menu->buttonOFF = SDL_DisplayFormatAlpha ( IMG_Load ( "./pictures/buttons/buttonOFF.png" ) );
	menu->buttonFont = TTF_OpenFont ( "./fonts/computerfont.ttf", 26 );
	menu->pMenu.x = x;		menu->pMenu.y = y;
	menu->pMenu.h = height;	menu->pMenu.w = width;

	for ( i = 0; i < elementsAmount; i++ )	{
		menu->elements[i].renderedElement = NULL;
		menu->elements[i].renderedElementHighlited = NULL;
		menu->elements[i].renderedElementHighlitedOFF = NULL;
		menu->elements[i].renderedElementOFF = NULL;
	}

	return menu;
}

void Menu_SetElement ( Menu *menu, int id, ButtonType type, char *label )
{
	if ( !menu )	return;

	if ( id >= menu->nbOfElements )	return;

	menu->elements[id].label = label;
	menu->elements[id].labelLenght = strlen ( label );
	switch ( type )
	{
	case MENU_STANDARD_BUTTON:
		menu->elements[id].renderedElement = renderButton ( IMG_Load ( "./pictures/buttons/standardButton.png" ), label, menu->buttonFont, NULL );
		menu->elements[id].renderedElementHighlited = renderButton ( IMG_Load ( "./pictures/buttons/standardButton_highlited.png" ), label, menu->buttonFont, NULL );
		break;
	case MENU_ON_OFF:
		menu->elements[id].renderedElement = renderButton ( IMG_Load ( "./pictures/buttons/standardButton.png" ), label, menu->buttonFont, menu->buttonON );
		menu->elements[id].renderedElementOFF = renderButton ( IMG_Load ( "./pictures/buttons/standardButton.png" ), label, menu->buttonFont, menu->buttonOFF );
		menu->elements[id].renderedElementHighlited = renderButton ( IMG_Load ( "./pictures/buttons/standardButton_highlited.png" ), label, menu->buttonFont, menu->buttonON );
		menu->elements[id].renderedElementHighlitedOFF = renderButton ( IMG_Load ( "./pictures/buttons/standardButton_highlited.png" ), label, menu->buttonFont, menu->buttonOFF );
		break;
	case MENU_TEXTFIELD:
		break;
	}
	menu->elements[id].type = type;
	menu->elements[id].toggled = 0;
	menu->elements[id].pElement.x = menu->pMenu.x + menu->pMenu.w / 2 - menu->elements[id].renderedElement->w / 2;
	menu->elements[id].pElement.y = menu->pMenu.y + ((menu->pMenu.h - menu->nbOfElements * menu->elements[id].renderedElement->h) / menu->nbOfElements * (id + 1)) + menu->elements[id].renderedElement->h * id;
	menu->elements[id].pElement2.x = menu->elements[id].pElement.x;
	menu->elements[id].pElement2.y = menu->elements[id].pElement.y;
	Menu_ProcessMouseHover ( menu, 0, 0 );
}

void Menu_ChangeElement ( Menu *menu, int id, ButtonType type, char *label )
{
	/* Delete the element */
	free ( menu->elements[id].label );
	SDL_FreeSurface ( menu->elements[id].renderedElement );
	SDL_FreeSurface ( menu->elements[id].renderedElementHighlited );
	if ( menu->elements[id].type == MENU_ON_OFF )	{
		SDL_FreeSurface ( menu->elements[id].renderedElementHighlitedOFF );
		SDL_FreeSurface ( menu->elements[id].renderedElementOFF );
	}

	/* Create the new one */
	Menu_SetElement ( menu, id, type, label );
}
void Menu_AddElement ( Menu *menu, ButtonType type, char *label )
{
	//MenuE
}

void Menu_ProcessMouseHover ( Menu *menu, int x, int y )
{
	int i = 0;
	Uint8 r = 0, g = 0, b = 0, a = 0;
	static int alreadyHovered = 0;

	if ( !menu )	return;

	for ( i = 0; i < menu->nbOfElements; i++ )	{
		if ( !menu->elements[i].renderedElement )	continue;
		if ( x >= menu->elements[i].pElement.x && x <= menu->elements[i].pElement.x + menu->elements[i].renderedElement->w &&
			 y >= menu->elements[i].pElement.y && y <= menu->elements[i].pElement.y + menu->elements[i].renderedElement->h )	{
			SDL_LockSurface ( menu->elements[i].renderedElement );
			SDL_GetRGBA ( SDL_GetPixel ( menu->elements[i].renderedElement,
										 x - menu->elements[i].pElement.x,
										 y - menu->elements[i].pElement.y ), 
						  menu->elements[i].renderedElement->format, &r, &g, &b, &a );
			if ( a >= 200 )	{
				menu->elements[i].hovered = 1;
				if ( !alreadyHovered )
					Mix_PlayChannel ( SOUND_CLICK, Menu_GetClickSound (), 0 );
				alreadyHovered = 1;
			}
			else
				alreadyHovered = menu->elements[i].hovered = 0;
			SDL_UnlockSurface ( menu->elements[i].renderedElement );
		}
		else 
			menu->elements[i].hovered = 0;
	}
}

int  Menu_ProcessMouseClick ( Menu *menu, int x, int y )
{
	if ( !menu )
		return -2;
	int i = 0;
	Menu_ProcessMouseHover ( menu, x, y );
	for ( i = 0; i < menu->nbOfElements; i++ )	{
		if ( menu->elements[i].hovered )	{
			Mix_PlayChannel ( SOUND_CLICK, Menu_GetClickSound (), 0 );
			if ( menu->elements[i].type != MENU_STANDARD_BUTTON )
				menu->elements[i].toggled = !menu->elements[i].toggled;
			else
				return i;
		}
	}
	return -1;
}

void Menu_DrawMenu ( Menu *menu, SDL_Surface *dst )
{
	int i = 0;
	for ( i = 0; i < menu->nbOfElements; i++ )	{
		if ( !menu->elements[i].renderedElement )	continue;
		if ( menu->elements[i].hovered && menu->elements[i].toggled )
			SDL_BlitSurface ( menu->elements[i].renderedElementHighlitedOFF, NULL, dst, &(menu->elements[i].pElement) );
		else if ( menu->elements[i].hovered && !menu->elements[i].toggled )
			SDL_BlitSurface ( menu->elements[i].renderedElementHighlited, NULL, dst, &(menu->elements[i].pElement) );
		else if ( !menu->elements[i].hovered && menu->elements[i].toggled )
			SDL_BlitSurface ( menu->elements[i].renderedElementOFF, NULL, dst, &(menu->elements[i].pElement) );
		else if ( !menu->elements[i].hovered && !menu->elements[i].toggled )
			SDL_BlitSurface ( menu->elements[i].renderedElement, NULL, dst, &(menu->elements[i].pElement) );
	}
}

void Menu_DeleteMenu ( Menu *menu )
{
	int i = 0;

	TTF_CloseFont ( menu->buttonFont );
	SDL_FreeSurface ( menu->buttonOFF );
	SDL_FreeSurface ( menu->buttonON );

	for ( i = 0; i < menu->nbOfElements; i++ )	{
		free ( menu->elements[i].label );
		SDL_FreeSurface ( menu->elements[i].renderedElement );
		SDL_FreeSurface ( menu->elements[i].renderedElementHighlited );
		if ( menu->elements[i].type == MENU_ON_OFF )	{
			SDL_FreeSurface ( menu->elements[i].renderedElementHighlitedOFF );
			SDL_FreeSurface ( menu->elements[i].renderedElementOFF );
		}
	}
	free ( menu->elements );
}
int  Menu_TypicMenuLoop ( Menu *menu, SDL_Surface *background, SDL_Surface *dst, int fps )
{
	SDL_Event event;
	int buttonClicked = -1, oldTime = SDL_GetTicks (), newTime = SDL_GetTicks (), counter = 0;
	char title[100] = "";

	while ( buttonClicked == -1 )	{
		SDL_WaitEvent ( &event );
			switch ( event.type )
			{
			case SDL_QUIT:
				return -1;
				break;
			case SDL_MOUSEMOTION:
				Menu_ProcessMouseHover ( menu, event.motion.x, event.motion.y );
				break;
			case SDL_MOUSEBUTTONDOWN:
				if ( event.button.button == SDL_BUTTON_LEFT )
					buttonClicked = Menu_ProcessMouseClick ( menu, event.button.x, event.button.y );
				break;
			}

		if ( (newTime = SDL_GetTicks ()) - oldTime >= 1000 )	{
			//sprintf ( title, "Tetris [FPS = %lf]", (double)counter * (double)(1000.0 / (double)(newTime - oldTime)) );
			//sprintf ( title, "Tetris [FPS = %d]", counter );
			counter = 0;
			oldTime = newTime;
			//SDL_WM_SetCaption ( title, NULL );
		}

		counter++;

		SDL_BlitSurface ( background, NULL, dst, NULL );
		Menu_DrawMenu ( menu, dst );

		SDL_Flip ( dst );
		SDL_Delay ( 1000 / fps );
	}

	return buttonClicked;
}
int  Menu_TypicMenuLoopAnimated ( Menu *menu, SDL_Surface *background, SDL_Surface *dst, int fps )
{
	SDL_Event event;
	int buttonClicked = -1, oldTime = SDL_GetTicks (), newTime = SDL_GetTicks (), counter = 0;
	char title[100] = "";

	while ( buttonClicked == -1 )	{
		SDL_WaitEvent ( &event );
			switch ( event.type )
			{
			case SDL_QUIT:
				return -1;
				break;
			case SDL_MOUSEMOTION:
				Menu_ProcessMouseHover ( menu, event.motion.x, event.motion.y );
				break;
			case SDL_MOUSEBUTTONDOWN:
				if ( event.button.button == SDL_BUTTON_LEFT )
					buttonClicked = Menu_ProcessMouseClick ( menu, event.button.x, event.button.y );
				break;
			}

		if ( (newTime = SDL_GetTicks ()) - oldTime >= 1000 )	{
			//sprintf ( title, "Tetris [FPS = %lf]", (double)counter * (double)(1000.0 / (double)(newTime - oldTime)) );
			//sprintf ( title, "Tetris [FPS = %d]", counter );
			counter = 0;
			oldTime = newTime;
			//SDL_WM_SetCaption ( title, NULL );
		}

		counter++;

		SDL_BlitSurface ( background, NULL, dst, NULL );
		Menu_DrawMenu ( menu, dst );

		SDL_Flip ( dst );
		SDL_Delay ( 1000 / fps );
	}

	return buttonClicked;
}
int  Menu_AnimateMenu ( Menu *menu, int time )
{
	int i = 0;
	
	for ( i = 0; i < menu->nbOfElements; i++ )	{
		
	}

	return 0;
}
Mix_Chunk* Menu_GetClickSound ()
{
	static Mix_Chunk *sound = NULL;

	if ( sound )	return sound;

	sound = Mix_LoadWAV ( "./sound/click.ogg" );

	return sound;
}