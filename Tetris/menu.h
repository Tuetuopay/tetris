/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/


#ifndef _MENU_H
#define _MENU_H

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

typedef enum ButtonType {
	MENU_STANDARD_BUTTON, MENU_ON_OFF, MENU_TEXTFIELD
};
typedef enum ButtonStatus {
	OFF, ON
};

typedef struct ElementMenu ElementMenu;
struct ElementMenu
{
	/* Element's type. Can take 3 values:
	 *  MENU_STANDARD_BUTTON: when clicking on the element, menu exits and returns the id of the clicked
	 *		element
	 *  MENU_ON_OFF: element which can take two values, enabled or not, for example, enable the music or
	 * not. Status stored in the toggled variable.
	 *  MENU_TEXTFIELD:	text area where the user enters a text, as his name. Size is stored in
	 * textfieldMaxSize, text stored in *textfieldValue
	 * /!\ NOT IMPLEMENTED /!\
	 **/
	ButtonType type;

	/* Text shown as element's description.
	 * Example: "Enter your name", "Play", "Enable Music ?"
	 **/
	char *label;
	int labelLenght;

	/* When a MENU_ON_oFF element, store if ON or OFF
	 **/
	int toggled;

	/* Stores informations from a MENU_TEXTFIELD element
	 **/
	int textfieldMaxSize;
 	char *textfieldValue;

	/* Spare informations about the element
	 * pElement2 is used when the menu is animated to store destination coordinates, while pElement stores
	 * the actual position
	 **/
	SDL_Rect pElement;
	SDL_Rect pElement2;
	int hovered;

	/* Stores the rendered element
	 **/
	SDL_Surface *renderedElement;
	SDL_Surface *renderedElementOFF;
	SDL_Surface *renderedElementHighlited;
	SDL_Surface *renderedElementHighlitedOFF;
};

typedef struct Menu Menu;
struct Menu
{
	/* Stores the elements amount that the menu stores
	 **/
	int nbOfElements;

	/* Stores the spares elements of the menu
	 **/
	ElementMenu *elements;

	/* Surfaces used when rendering buttons, stores the red or green circles showed at the right of a
	 * MENU_ON_OFF button.
	 **/
	SDL_Surface *buttonON;
	SDL_Surface *buttonOFF;

	/* Spares surfaces, like the menu background
	 **/
	SDL_Surface *menuBG;
//	SDL_Surface *cursor;	// inutilis�

	/* Position and dimentions of the menu
	 **/
 	SDL_Rect pMenu;

	/* Font used for rendering buttons
	 **/
	TTF_Font *buttonFont;
};

/********************************************************************************************************
 * Passons maintenant aux prototypes des fonctions pour la cr�ation de menu.				*
 * Celles ci sont utilis�es dans les fonctions de construction, elles n'ont normalement pas besoin	*
 * d'�tre utilis�es directement.									*
 ********************************************************************************************************/
/*	void renderStandardButton ( Menu *menu, int id );
	
	int initMenu ( Menu *menu, char backgroundPath[], SDL_Rect pMenu );
	void deleteMenu ( Menu *menu );
	int addElement ( Menu *menu, int type, char label[] ); */
	
SDL_Surface* renderButton ( SDL_Surface *btnBG, char text[], TTF_Font *police, SDL_Surface *btnOnOff );
Menu *Menu_CreateMenu ( int width, int height, int x, int y, int elementsAmount );
void Menu_SetElement ( Menu *menu, int id, ButtonType type, char *label );
void Menu_ChangeElement ( Menu *menu, int id, ButtonType type, char *label );
void Menu_AddElement ( Menu *menu, ButtonType type, char *label );
void Menu_ProcessMouseHover ( Menu *menu, int x, int y );
int  Menu_ProcessMouseClick ( Menu *menu, int x, int y );
void Menu_DrawMenu ( Menu *menu, SDL_Surface *dst );
void Menu_DeleteMenu ( Menu *menu );
int  Menu_TypicMenuLoop ( Menu *menu, SDL_Surface *background, SDL_Surface *dst, int fps );
Mix_Chunk* Menu_GetClickSound ();

#endif







