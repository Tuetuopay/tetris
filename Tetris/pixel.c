/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/

#include "main.h"

Uint32 SDL_GetPixel ( SDL_Surface *src, int x, int y )
{
	int bpp = src->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)src->pixels + y * src->pitch + x * bpp;
	
	switch ( bpp )
	{
		case 1:
			return *p;
		case 2:
			return *(Uint16 *)p;
		case 3:
			if ( SDL_BYTEORDER == SDL_BIG_ENDIAN )
				return p[0] << 16 | p[1] << 8 | p[2];
			else
				return p[0] | p[1] << 8 | p[2] << 16;
		case 4:
			return *(Uint32 *)p;
		default:
			return 0;
	}
}
void SDL_SetPixel ( SDL_Surface *dst, int x, int y, Uint32 pixel )
{
	int bpp = dst->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)dst->pixels + y * dst->pitch + x * bpp;
	
	switch ( bpp )
	{
		case 1:
			*p = pixel;
			return;
		case 2:
			*(Uint16 *)p = pixel;
			return;
		case 3:
			p[1] = (pixel >> 8) & 0xff;
			if ( SDL_BYTEORDER == SDL_BIG_ENDIAN )	{
				p[0] = (pixel >> 16) & 0xff;
				p[2] = pixel & 0xff;
			} else	{
				p[0] = pixel & 0xff;
				p[2] = (pixel >> 16) & 0xff;
			}
			return;
		case 4:
			*(Uint32 *)p = pixel;
			return;
	}
}
void SDL_MyBlit ( SDL_Surface *src, SDL_Rect *clipper, SDL_Surface *dst, SDL_Rect *pos )
{
	SDL_Rect pTemp = {0}, pSec = {0};
	Uint8 r, g, b, a;
	
	//printf ( "SDL_MyBlit called. args :\n" );
	//printf ( "\tsrc     = %x\n", src );
	//printf ( "\t |->w = %d\n\t |->h = %d\n", src->w, src->h );
	//printf ( "\tclipper = %x\n", clipper );
	if ( clipper )	{
		//printf ( "\t |->w = %d\n\t |->h = %d\n\t |->x = %d\n\t |->y = %d\n", clipper->w, clipper->h, clipper->x, clipper->y );
	}
	//printf ( "\tdst     = %x\n", dst );
	//printf ( "\tpos     = %x\n", pos );
	if ( pos )  {
		//printf ( "\t |->w = %d\n\t |->h = %d\n\t |->x = %d\n\t |->y = %d\n", pos->w, pos->h, pos->x, pos->y );
	}
	
	if ( !pos )	{
		pSec.x = 0;
		pSec.y = 0;
		pos = &pSec;
	}
	if ( !src ) return;
	if ( !dst ) return;
	
	SDL_LockSurface ( src );
	SDL_LockSurface ( dst );
	
	for ( pTemp.y = pos->y; pTemp.y < pos->y + src->h; pTemp.y++ )	{
		for ( pTemp.x = pos->x; pTemp.x < pos->x + src->w; pTemp.x++ )	{
			SDL_GetRGBA ( SDL_GetPixel ( src, pTemp.x, pTemp.y ), src->format, &r, &g, &b, &a );
			SDL_SetPixel ( dst, pTemp.x, pTemp.y, SDL_MapRGBA ( dst->format, r, g, b, a ));
		}
	}
	
	SDL_UnlockSurface ( src );
	SDL_UnlockSurface ( dst );
}

void SDL_MySetAlpha ( SDL_Surface *dst, Uint8 alpha )
{
	int x = 0, y = 0;
	Uint8 r, g, b, a;

	SDL_LockSurface ( dst );

	for ( y = 0; y < dst->h; y++ )	{
		for ( x = 0; x < dst->w; x++ )	{
			SDL_GetRGBA ( SDL_GetPixel ( dst, x, y ), dst->format, &r, &g, &b, &a );
			SDL_SetPixel ( dst, x, y, SDL_MapRGBA ( dst->format, r, g, b, (alpha > a) ? a : alpha ) );
		}
	}

	SDL_UnlockSurface ( dst );
}