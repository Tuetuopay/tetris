#ifndef PIXEL_H
#define PIXEL_H

#include <SDL/SDL.h>

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	#define MASQUE_ROUGE	0xff000000
	#define MASQUE_VERT	0x00ff0000
	#define MASQUE_BLEU	0x0000ff00
	#define MASQUE_ALPHA	0x000000ff
#else
	#define MASQUE_ROUGE    0x000000ff
	#define MASQUE_VERT     0x0000ff00
	#define MASQUE_BLEU     0x00ff0000
	#define MASQUE_ALPHA    0xff000000
#endif

Uint32 SDL_GetPixel ( SDL_Surface *src, int x, int y );
void SDL_SetPixel ( SDL_Surface *dst, int x, int y, Uint32 pixel );
void SDL_MyBlit ( SDL_Surface *src, SDL_Rect *clipper, SDL_Surface *dst, SDL_Rect *pos );
void SDL_MySetAlpha ( SDL_Surface *dst, Uint8 alpha );

#endif
