/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/

#include "main.h"
#include "system.h"

unsigned char * Sys_GetUStringValueInFile ( char filename[], char name[] )
{
	char buf[2000] = "",
		*label = NULL;
	unsigned char *value = NULL;
	FILE *file = NULL;
	int valueLength = 0, i = 0, valueIndex = 0;

	if ( !(file = fopen ( filename, "rb" )) )	{
		fprintf ( stderr, "Unable to load file \"%s\", file is missing or locked.\n", filename );
		return NULL;
	}
	if ( name[strlen(name) - 1] != ':' )	{
		label = (char*)malloc ( strlen ( name ) + 5 );
		sprintf ( label, "%s:\0", name );
	}
	else	label = name;

	while ( fgets ( buf, 1990, file ) )	{
		if ( strstr ( buf, label ) )	{
			//fseek ( file, -10, SEEK_CUR );
			i = 0;
			while ( buf[i] != ' ' )	i++;
			while ( buf[i + valueLength] != '\n' )	valueLength++;
			if ( !(value = (unsigned char*)malloc ( (valueLength + 4) * sizeof(unsigned char) )) )
				fprintf ( stderr, "Sys_GetUStringValueInFile(%s,%s): Unable to allocate %d bytes of memory.\n",
					filename, name, (valueLength * 2) * sizeof(int) );
			else	{
				//fseek ( file, -(valueLength + 1), SEEK_CUR );
				valueIndex = i + 1;
				for ( i = 0; i < valueLength; i++ )	{
					value[i] = buf[valueIndex + i];
					if ( value[i] == '\n' || value[i] == '\r' )
						value[i] = '\0';
				}
				value[valueLength+1] = '\0';
			}
			fclose ( file );
			return value;
		}
	}

	fclose ( file );

	fprintf ( stderr, "Sys_GetUStringValueInFile(%s,%s): Option doesn't exists.\n", filename, name );
	return NULL;
}

char * Sys_GetStringValueInFile ( char filename[], char name[] )
{
	int i = 0, length = 0;
	char *toReturn = NULL;
	unsigned char* value = NULL;

	value = Sys_GetUStringValueInFile ( filename, name );

	while ( value[length] != '\0' )	length++;

	if ( !(toReturn = (char*)malloc ( (length+1) * sizeof(char) )) )	{
		fprintf ( stderr, "Sys_GetStringValueInFile(%s,%s): Unable to allocate %d bytes of memory.\n",
			filename, name, (length+1) * sizeof(char) );
	}
	for ( i = 0; i <= length; i++ )	toReturn[i] = value[i];

	free ( value );

	return toReturn;
}

int Sys_GetIntValueInFile ( char filename[], char name[] )
{
	char *value = Sys_GetStringValueInFile ( filename, name );
	int intValue = 0;

	if ( !value )	return -1;

	sscanf ( value, "%ld", &intValue );

	free ( value );

	return intValue;
}
void Sys_SaveStringInFile ( char filename[], char name[], char value[] )
{
	char buf[2000] = "",
		*label = NULL,
		*srcFilename = NULL;
	FILE *srcFile = NULL,
		*dstFile = NULL;
	int labelReached = 0;

	/* Getting the source filename
	 * Renaming the file from <file.ext> to <file.ext2>
	 **/
	if ( !(srcFilename = (char*)malloc ( (strlen ( filename ) + 4) * sizeof(char) )) )	{
		fprintf ( stderr, "Unable to allocate %d bytes of memory.\n", (strlen ( filename ) + 4) * sizeof(char) );
		return;
	}
	sprintf ( srcFilename, "%s2", filename );
	rename ( filename, srcFilename );

	/* Now opening the source file */
	if ( !(srcFile = fopen ( srcFilename, "r" )) )	{
		fprintf ( stderr, "Unable to load file \"%s\", file is missing or locked.\n", srcFilename );
		free ( srcFilename );
		return;
	}

	/* Then the destination file */
	if ( !(dstFile = fopen ( filename, "w" )) )	{
		fprintf ( stderr, "Unable to load file \"%s\", file is missing or locked.\n", filename );
		return;
	}

	/* Now making the label */
	if ( name[strlen(name) - 1] != ':' )	{
		label = (char*)malloc ( strlen ( name ) + 5 );
		sprintf ( label, "%s:", name );
	}
	else	label = name;

	rewind ( srcFile );

	while ( fgets ( buf, 1990, srcFile ) )	{
		if ( strstr ( buf, label ) )	{	/* If we are at the good label */
			fprintf ( dstFile, "%s %s\n", label, value );
			labelReached = 1;
		}
		else	{	/* Wrong label, just write it down and skip */
			fputs ( buf, dstFile );
			//fputc ( '\n', dstFile );
		}
	}

	if ( !labelReached )	{
		fprintf ( dstFile, "%s %s\n", label, value );
		fprintf ( stderr, "Sys_SaveStringInFile(%s,%s,%s): Option doesn't exists, created it.\n", filename, name, value );
	}

	fclose ( srcFile );
	fclose ( dstFile );
	remove ( srcFilename );
	free ( srcFilename );
	free ( label );
}

void Sys_SaveIntInFile ( char filename[], char name[], int value )
{
	char buf[2000] = "",
		*label = NULL,
		*srcFilename = NULL;
	FILE *srcFile = NULL,
		*dstFile = NULL;
	int labelReached = 0;

	/* Getting the source filename
	 * Renaming the file from <file.ext> to <file.ext2>
	 **/
	if ( !(srcFilename = (char*)malloc ( (strlen ( filename ) + 4) * sizeof(char) )) )	{
		fprintf ( stderr, "Unable to allocate %d bytes of memory.\n", (strlen ( filename ) + 4) * sizeof(char) );
		return;
	}
	sprintf ( srcFilename, "%s2", filename );
	rename ( filename, srcFilename );

	/* Now opening the source file */
	if ( !(srcFile = fopen ( srcFilename, "r" )) )	{
		fprintf ( stderr, "Unable to load file \"%s\", file is missing or locked.\n", srcFilename );
		free ( srcFilename );
		return;
	}

	/* Then the destination file */
	if ( !(dstFile = fopen ( filename, "w" )) )	{
		fprintf ( stderr, "Unable to load file \"%s\", file is missing or locked.\n", filename );
		return;
	}

	/* Now making the label */
	if ( name[strlen(name) - 1] != ':' )	{
		label = (char*)malloc ( strlen ( name ) + 5 );
		sprintf ( label, "%s:", name );
	}
	else	label = name;

	rewind ( srcFile );

	while ( fgets ( buf, 1990, srcFile ) )	{
		if ( strstr ( buf, label ) )	{	/* If we are at the good label */
			fprintf ( dstFile, "%s %d\n", label, value );
			labelReached = 1;
		}
		else	{	/* Wrong label, just write it down and skip */
			fputs ( buf, dstFile );
			//fputc ( '\n', dstFile );
		}
	}

	if ( !labelReached )	{
		fprintf ( dstFile, "%s %d\n", label, value );
		fprintf ( stderr, "Sys_SaveIntInFile(%s,%s,%d): Option doesn't exists, created it.\n", filename, name, value );
	}

	fclose ( srcFile );
	fclose ( dstFile );
	remove ( srcFilename );
	free ( srcFilename );
	free ( label );
}