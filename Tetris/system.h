/*************************************************************************************
 *       __________  __________  __________  __________  __________  __________      *
 *      /___   ___/ /  _______/ /___   ___/ /  ____   / /___   ___/ /  _______/      *
 *         /  /    /  /______      /  /    /  /__ /  /     /  /    /  /______        *
 *        /  /    /  _______/     /  /    /  __   __/     /  /    /_______  /        *
 *       /  /    /  /______      /  /    /  /  | |   ____/  /___  ______/  /         *
 *      /__/    /_________/     /__/    /__/   |_|  /__________/ /________/          *
 *                                                                                   *
 *************************************************************************************/

#ifndef _SYSTEM_H
#define _SYSTEM_H

/* Returns the string value in a tag from the given file
 *  - <filename> is the path to the file to read
 *  - <name> is the tag to read in the file
 * The tag inside the file must be formatted this way:
 * "<tag>: <value>\n"
 * If no tag is found or memory allocation fails, it will return NULL
 **/
unsigned char * Sys_GetUStringValueInFile ( char filename[], char name[] );
char * Sys_GetStringValueInFile ( char filename[], char name[] );


/* Returns the intger value in a tag from the given file
 *  - <filename> is the path to the file to read
 *  - <name> is the tag to read in the file
 * Makes an internal call to Sys_GetStringValueInFile (), if this funciton
 * returns NULL, will return -1
 **/
int Sys_GetIntValueInFile ( char filename[], char name[] );

/* Saves a string in a file for a tag
 *  - <filename> path to the file to write in
 *  - <name> tag to write in front of
 *  - <value> string to write for the tag
 **/
void Sys_SaveStringInFile ( char filename[], char name[], char value[] );


/* Saves an integer in a file for a tag
 *  - <filename> path to the file to write in
 *  - <name> tag to write in front of
 *  - <value> integer to write for the tag
 **/
void Sys_SaveIntInFile ( char filename[], char name[], int value );

#endif